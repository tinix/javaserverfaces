package beans.backing;

import beans.model.Candidato;
import javax.annotation.ManagedBean;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.inject.Inject;

/**
 *
 * @author tinix
 */
@ManagedBean
@Named(value = "vacanteForm")
@RequestScoped
public class VacanteForm {

    /**
     * Creates a new instance of VacanteForm
     */
    public VacanteForm() {
    }
    @Inject
    @ManagedProperty(value = "#{candidato}")
    
    private Candidato candidato;
    
    public Candidato getCandidato(){
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }
    
    public String enviar(){
        if (this.candidato.getNombre().equals("Juan")) {
            return "exito";
        } else {
            return "fallo";
        }
    }
}





